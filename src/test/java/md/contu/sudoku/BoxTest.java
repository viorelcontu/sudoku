package md.contu.sudoku;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;


public class BoxTest {
    private final int row = 1;
    private final int col = 1;
    private Box box;

    @BeforeEach
    public void setUp() {
        box = new Box(row, col);
    }

    @Test
    public void getPossibles_OnlyReturnsAvailableNumbers() {
        Set<Integer> possibles = box.getPossibles();
        assertEquals(new HashSet<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9)), possibles);

        box.delPossible(7);
        assertEquals(new HashSet<>(Arrays.asList(1, 2, 3, 4, 5, 6, 8, 9)), box.getPossibles());
    }

    @ParameterizedTest(name = "run #{index} with [{arguments}]")
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9})
    public void getValue(int value) {
        box.setValue(value);
        assertEquals(value, box.getValue());
    }

    @Test
    public void isSolved_isTrueOnlyWhenOnePossibleLeft() {
        assertFalse(box.isSolved());
        IntStream.rangeClosed(1, 8).forEach(box::delPossible);
        assertTrue(box.isSolved());
    }

    @Test
    public void size_ShouldBeEqualToNumberOfPossibles() {
        assertEquals(9, box.size());
        box.delPossible(4);
        assertEquals(8, box.size());
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9})
    public void delPossible(int value) {
        box.delPossible(value);
        assertFalse(box.getPossibles().contains(value));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8, 9})
    public void delPossible_ShouldFailWhenRemovingLastPossible(int value) {
        box.setValue(value);
        assertThrows(RuntimeException.class, () -> box.delPossible(value));
    }


    @Test
    public void setValue() {
        box.setValue(1);
        assertEquals(1, box.getValue());
    }

    @Test
    public void isRelatedWith_ShouldReturnTrueToBoxesOnSameRowColumnSquare() {
        final int otherRow = 8;
        final int otherCol = 8;
        assertTrue(box.isRelatedWith(new Box(row, otherCol)));
        assertTrue(box.isRelatedWith(new Box(otherRow, col)));
        assertTrue(box.isRelatedWith(new Box(row + 1, col + 1)));
        assertFalse(box.isRelatedWith(new Box(otherRow, otherCol)));
    }
}