package md.contu.sudoku;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SudokuTest {

    /*
    private SudokuInput sudokuInput = Mockito.mock(SudokuInput.class);
    private Sudoku sudoku;
    private int solution[][];

    @BeforeEach
    void setUp() {
        sudoku = new Sudoku(sudokuInput);
    }

    @AfterEach
    void cleanUp() {
        sudoku = null;
        solution = null;
    }

    private int[][] getInput() {
        return new int[][]{
                {0, 0, 6, 0, 0, 0, 0, 0, 4},
                {0, 0, 0, 8, 6, 0, 7, 3, 0},
                {0, 4, 0, 3, 5, 0, 0, 0, 2},

                {1, 7, 0, 4, 0, 0, 6, 0, 0},
                {0, 9, 0, 0, 0, 0, 0, 8, 0},
                {0, 0, 8, 0, 0, 6, 0, 1, 7},

                {2, 0, 0, 0, 8, 1, 0, 4, 0},
                {0, 6, 7, 0, 4, 3, 0, 0, 0},
                {8, 0, 0, 0, 0, 0, 3, 0, 0}
        };
    }

    private int[][] getCorrectSolution() {
        return new int[][]
                {
                        {3, 8, 6, 2, 1, 7, 5, 9, 4},
                        {5, 2, 9, 8, 6, 4, 7, 3, 1},
                        {7, 4, 1, 3, 5, 9, 8, 6, 2},

                        {1, 7, 3, 4, 2, 8, 6, 5, 9},
                        {6, 9, 2, 1, 7, 5, 4, 8, 3},
                        {4, 5, 8, 9, 3, 6, 2, 1, 7},

                        {2, 3, 5, 7, 8, 1, 9, 4, 6},
                        {9, 6, 7, 5, 4, 3, 1, 2, 8},
                        {8, 1, 4, 6, 9, 2, 3, 7, 5}
                };
    }
    */

    /*
    private void printSolution () {
        for (int row = 0; row < 9; row++) {
            if (row > 0 && row % 3 == 0) System.out.println();
            for (int col = 0; col < 9; col++)
                System.out.print((col > 0 && col % 3 == 0 ? "  " : " ") + solution[row][col]);
            System.out.println();
        }
    }
    */

    /*
    private boolean verifySolution() {
        int[][] correct = getCorrectSolution();
        for (int i = 0; i < 9; i++) {
            if (!Arrays.equals(solution[i], correct[i])) return false;
        }
        return true;
    }

    @Test
    void solve() {
        Mockito.when(sudokuInput.getInput()).thenReturn(getInput());
        solution = sudoku.solve();
        Mockito.verify(sudokuInput).getInput();
        Mockito.verifyNoMoreInteractions(sudokuInput);
        assertTrue(verifySolution());
    }

    @Test
    void validateInput_wrongRowSizeShouldThrowError() {
        int[][] input = new int[10][9];
        Mockito.when(sudokuInput.getInput()).thenReturn(input);
        assertThrows(IllegalArgumentException.class, () -> sudoku.solve());
    }

    @Test
    void validateInput_wrongColSizeShouldThrowError() {
        int[][] input = new int[9][3];
        Mockito.when(sudokuInput.getInput()).thenReturn(input);
        assertThrows(IllegalArgumentException.class, () -> sudoku.solve());
    }

    @Test
    void validateInput_wrongDataShouldThrowError() {
        int[][] input = getInput();
        input[1][1] = 100;
        Mockito.when(sudokuInput.getInput()).thenReturn(input);
        assertThrows(IllegalArgumentException.class, () -> sudoku.solve());
    }

    */
}