package md.contu.sudoku;

import lombok.Getter;
import md.contu.sudoku.loaders.SudokuLoader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class Sudoku {
    public final static int SIZE = 9;
    public final static int GRID_SIZE = SIZE * SIZE;
    private int[][] input;

    @Getter
    private List<Box> grid;

    private static int calculateIndex(int row, int col) {
        return row * SIZE + col;
    }

    public Sudoku (SudokuLoader loader) {
        this.input = loader.getInput();
        validateInput();
        loadValues();
        validateSudoku();
    }

    private void validateInput() {
        final String sizeException = "Wrong Sudoku Matrix Size, must be 9x9";

        if (input == null || input.length != SIZE)
            throw new IllegalArgumentException(sizeException);

        for (int row = 0; row < SIZE; row++) {
            if (input[row] == null || input[row].length != SIZE)
                throw new IllegalArgumentException(sizeException);

            for (int col = 0; col < SIZE; col++) {
                if (input[row][col] < 0 || input[row][col] > 9)
                    throw new IllegalArgumentException("Wrong Sudoku entrance data, must be between 1 and 9");
            }
        }
    }

    private void loadValues() {
        grid = new ArrayList<>();
        for (int row = 0; row < SIZE; row++)
            for (int col = 0; col < SIZE; col++) {
                grid.add(calculateIndex(row, col), new Box(row, col));

                if (input[row][col] != 0)
                    grid.get(calculateIndex(row, col)).setValue(input[row][col]);
            }
    }

    private void validateSudoku() {
        //TODO verify if there are no numbers repeating in the same vertical, horizontal square
    }

    public Collection<List<Box>> relatedBoxes(boolean includeSolved) {
        List<Function<Box, Integer>> groupingCategories = Arrays.asList(Box::getRow, Box::getCol, Box::getSqr);
        Collection<List<Box>> related = new ArrayList<>();

        for (Function <Box, Integer> category : groupingCategories)
            related.addAll(grid.stream()
                    .filter(box -> includeSolved || !box.isSolved())
                    .collect(groupingBy(category))
                    .values());
        return related;
    }

/*
    public int[][] getSolution() {
        int[][] solution = new int[SIZE][SIZE];

        for (int row = 0; row < SIZE; row++)
            for (int col = 0; col < SIZE; col++)
                solution[row][col] = grid.get(calculateIndex(row, col)).getValue();

        return solution;
    }
*/

    public List<Box> getUnsolvedBoxes() {
        return grid.stream()
                .filter(box -> !box.isSolved())
                .sorted(Comparator.comparing(Box::size))
                .collect(toList());
    }

    public void printSolution() {
        for (int row = 0; row < SIZE; row++) {
            if (row > 0 && row % 3 == 0) System.out.println();
            for (int col = 0; col < SIZE; col++)
                System.out.print((col > 0 && col % 3 == 0 ? "  " : " ") + grid.get(calculateIndex(row, col)));
            System.out.println();
        }
    }

    public boolean isSolved() {
        return grid.stream().allMatch(Box::isSolved);
    }

}
