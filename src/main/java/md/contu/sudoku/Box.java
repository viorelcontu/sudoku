package md.contu.sudoku;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Set;
import java.util.stream.IntStream;

import static java.lang.Integer.bitCount;
import static java.util.stream.Collectors.toSet;

@RequiredArgsConstructor
@EqualsAndHashCode
public class Box {
    private short possibles = 0B111_111_111;

    @Getter
    final private int row;
    @Getter
    final private int col;

    public int getSqr() {
        return 3 * (row / 3) + (col / 3);
    }

    @Override
    public String toString() {
        return "" + (isSolved() ? getValue() : "-");
    }

    public Set<Integer> getPossibles() {
        return IntStream.rangeClosed(1, 9)
                .filter(i -> (possibles >> (i - 1) & 1) == 1)
                .boxed()
                .collect(toSet());
    }

    public int getValue() {
        return isSolved() ? IntStream.rangeClosed(1, 9).filter(i -> (possibles >> (i-1) & 1) == 1).findFirst().orElse(0) : 0;
    }

    public boolean isSolved() {
        return size() == 1;
    }

    public int size() {
        return bitCount(possibles);
    }

    public boolean delPossible(int value) {
        if (size() == 1 && (possibles >> (value - 1) & 1) == 1)
            throw new RuntimeException("You are not allowed to delete the last possible");

        possibles &= ~(1 << (value - 1));
        return isSolved();
    }

    public void setValue(int value) {
        possibles = (short) (1 << (value - 1));
    }

    public boolean isRelatedWith(Box otherBox) {
        if (this.equals(otherBox)) return false;
        return (this.getRow() == otherBox.getRow()) || (this.getCol() == otherBox.getCol()) || (this.getSqr() == otherBox.getSqr());
    }
}
