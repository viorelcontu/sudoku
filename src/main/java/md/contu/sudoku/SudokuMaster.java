package md.contu.sudoku;

import md.contu.sudoku.loaders.TestLoader;
import md.contu.sudoku.loaders.SudokuLoader;
import md.contu.sudoku.solvers.BackTracker;
import md.contu.sudoku.solvers.CrossEliminator;
import md.contu.sudoku.solvers.SudokuSolver;

import java.util.Arrays;
import java.util.List;

public class SudokuMaster {
    public static void main (String [] args) {
        SudokuLoader loader = new TestLoader();
        Sudoku sudoku = new Sudoku(loader);

        List<SudokuSolver> solvers = Arrays.asList(new CrossEliminator(), new BackTracker());

        solvers.forEach(solver -> { if (!sudoku.isSolved()) solver.process(sudoku); });

        sudoku.printSolution();
    }
}
