package md.contu.sudoku.solvers;

import md.contu.sudoku.Box;
import md.contu.sudoku.Sudoku;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BackTracker implements SudokuSolver{

    public void process(Sudoku sudoku) {
        List<Box> unsolved = sudoku.getUnsolvedBoxes();

        Map<Box, Set<Integer>> impossibles = new HashMap<>();
        Map<Box, Integer> solutionCandidates = new HashMap<>();

        int index = 0;

        primaryLoop:
        while (index < unsolved.size()) {
            Box currBox = unsolved.get(index);

            Set<Integer> possibles = currBox.getPossibles();
            possibles.removeAll(impossibles.getOrDefault(currBox, Collections.emptySet()));

            for (int candidate : possibles) {
                boolean candidateIsValid = unsolved.stream()
                        .limit(index) // you only check until those already processed
                        .filter(box -> box.isRelatedWith(currBox))
                        .allMatch(box -> solutionCandidates.get(box) != candidate); //if some other related box has the same number already, then it is not valid
                //the line above might be error prone, need to test it when the filter leaves no items

                if (candidateIsValid) {
                    solutionCandidates.put(currBox, candidate);
                    index++;
                    continue primaryLoop;
                }
            }

            impossibles.remove(currBox); //before you return to previous, you need to clean up the mess
            // Backtrack to previous box and add another failure to impossibles
            Box prevBox = unsolved.get(--index);
            impossibles.putIfAbsent(prevBox, new HashSet<>()); //just in case we have no such key yet
            impossibles.get(prevBox).add(solutionCandidates.get(prevBox));
            solutionCandidates.remove(prevBox); //that was the wrong solution
        }

        unsolved.forEach(box -> box.setValue(solutionCandidates.get(box)));
    }
}
