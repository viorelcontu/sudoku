package md.contu.sudoku.solvers;

import md.contu.sudoku.Sudoku;

public interface SudokuSolver {
    void process(Sudoku sudoku);
}
