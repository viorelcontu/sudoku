package md.contu.sudoku.solvers;

import md.contu.sudoku.Box;
import md.contu.sudoku.Sudoku;

import java.util.*;
import java.util.function.Function;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.*;

public class CrossEliminator implements SudokuSolver {

    public void process(Sudoku sudoku) {
        Queue<Box> queue = sudoku.getGrid().stream()
                .filter(Box::isSolved)
                .collect(toCollection(LinkedList::new));

        do {
            while (!queue.isEmpty()) {

                Box solvedBox = queue.remove();
                int blockedPossible = solvedBox.getValue();

                queue.addAll(sudoku.getGrid().stream()
                        .filter(box -> !box.isSolved())
                        .filter(box -> box.isRelatedWith(solvedBox))
                        .filter(box -> box.delPossible(blockedPossible)) //two operations in one here!! not good! unreadable
                        .collect(toList()));
            }

            /*
                This needs to be in outer loop, so that we first clean as much as possible from out possibles grid,
                Then we search the isolated possibles per row, column and square to filter out more candidates.
                Otherwise we would run the search per every cross checked box
             */

            Map<Box, Integer> isolatedPossibles = searchIsolatedPossibles(sudoku);
            isolatedPossibles.forEach(Box::setValue);
            queue.addAll(isolatedPossibles.keySet());

        } while (!queue.isEmpty());

    }

    private Map<Box, Integer> searchIsolatedPossibles(Sudoku sudoku) {
        //returns the map of boxes with only one viable possible.
        //these are possibles that are isolated (only found once per row/column/square)

        Map<Box, Integer> boxSolutions = new HashMap<>();
        Collection<List<Box>> relatedBoxes = sudoku.relatedBoxes(false);
        relatedBoxes.forEach(listBox -> boxSolutions.putAll(isolatedPossibles(listBox)));
        return boxSolutions;
    }

    private Map<Box, Integer> isolatedPossibles(Collection<Box> boxes) {
        int[] counter = new int[Sudoku.SIZE]; //calculateIndex is the possible, value is count
        Box[] detected = new Box[Sudoku.SIZE]; //the last encountered box with that element in counter

        for (Box box : boxes)
            for (int possible : box.getPossibles())  {
                counter[possible - 1]++;
                detected[possible - 1] = box;
            }

        return IntStream.rangeClosed(1, 9)
                .filter(i -> counter[i - 1] == 1)
                .boxed()
                .collect(toMap(i -> detected[i - 1], Function.identity()));
    }

}
