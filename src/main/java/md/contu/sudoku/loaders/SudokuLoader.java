package md.contu.sudoku.loaders;

public interface SudokuLoader {
    int[][] getInput();
}
